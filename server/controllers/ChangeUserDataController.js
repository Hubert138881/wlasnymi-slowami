const inputValidation = require("../collectors/inputValidation")

module.exports = (server, app) => {
    server.post("/changeUserData", app.upload.fields([]), (req, res) => {
        return new Promise((resolve, reject) => {
            inputValidation(app, req).then(
                async () => {
                    let objectToSet = {$set: {}}

                    for(let field in req.body) objectToSet["$set"][field] = req.body[field]
                    delete objectToSet["$set"].confirmation

                    if(objectToSet["$set"].password) objectToSet["$set"].password = await app.bcrypt.hash(objectToSet["$set"].password, 10)

                    app.dbClient.db("test").collection("users").updateOne({"_id": new app.ObjectID(req.session.userId)}, objectToSet).then(
                        () => resolve(),
                        (err) => reject({msg: "CONNECTIONERROR", err})
                    )
                },
                (err) => reject(err)
            )
        }).then(
            () => res.send({err: false}),
            (err) => {
                switch(err.msg){
                    case "CONNECTIONERROR":
                        res.send({err: true})
                        break
                    case "INPUTERROR":
                        res.send({err: true, validations: err.err})
                        break
                    case "HASHERROR":
                        res.send({err: true})
                        break
                }
            }
        )
    })
}