const Validators = require("../collectors/Validators")

/**
 * @usage Check permissions in header and return a promise with permissions
 *
 * @resolve permissions object
 * @fail error with message
 */

function checkHeader(userRank, connection, db, collection, op) {
    return new Promise((resolve, reject) => {
        let permissions = {
            read: false,
            write: false
        }

        //Get collection's header from database
        connection.db(db).collection(collection).findOne({id: "header"}).then(
            (res) => {
                if (res) {
                    let neededPerms = {
                        read: false,
                        write: false
                    }
                    //Find out what permissions actually we need
                    if (op != "insertOne" && op != "insertMany") neededPerms.read = true
                    if (op != "findOne" && op != "find") neededPerms.write = true

                    //Get read/write roles stored in header (in "allowed" object), if * found - admit permissions
                    if (neededPerms.read) {
                        if (res.allowed) {
                            for (let rank of res.allowed.read) {
                                if (rank == "*") permissions.read = true
                                if (rank == userRank) permissions.read = true
                            }
                        }
                    }
                    if (neededPerms.write) {
                        if (res.allowed) {
                            for (let rank of res.allowed.write) {
                                if (rank == "*") permissions.write = true
                                if (rank == userRank) permissions.write = true
                            }
                        }
                    }
                    resolve(permissions)
                }
                else resolve(null)
            },
            (err) => reject({msg: "NOCONNECTION", err})
        )
    })
}

function checkAccess(document, userRank, userId, type) {
    if (type == "findOne") type = "find"
    else if (type == "deleteOne" || type == "deleteMany") type = "delete"
    else if (type == "updateOne" || type == "updateMany") type = "update"

    if (type == "find" || type == "update") {
        for (let rank in document.allowed[type][0])
            if (rank == userRank) return ({hasAccessToFollowing: false, fields: document.allowed[type][0][rank]})
        for (let id in document.allowed[type][1])
            if (id == userId) return ({hasAccessToFollowing: false, fields: document.allowed[type][1][id]})
        if (userId) return ({hasAccessToFollowing: true, fields: document.allowed[type][2]})
        else return ({hasAccessToFollowing: true, fields: document.allowed[type][3]})
    }
    else if (type == "delete") {
        console.log(document.allowed.delete, userId)
        for (let rank of document.allowed.delete[0]) if (rank == userRank) return (true)
        for (let id of document.allowed.delete[1]) if (id == userId) return (true)
        return (false)
    }
}

/**
 * @usage This function checks read/write permissions of many documents. If any document doesn't provide sufficient permissions
 * then set it in the permissions object to false
 *
 * @return Permissions object
 */

function checkConnectionOfMany(userRank, userId, connection, db, collection, query, operation, options) {
    return new Promise((resolve, reject) => {

        let findOp = connection.db(db).collection(collection).find(query)

        if(options) {
            if (options.sort && operation == "find") findOp = findOp.sort(options.sort)
            if (options.limit && operation == "find") findOp = findOp.limit(options.limit)
        }

        findOp.toArray().then(
            (documents) => {
                let final = [], Aflag = false

                for (let document of documents) {
                    if (document.allowed) {
                        let access = checkAccess(document, userRank, userId, operation)

                        let validObject

                        if (operation == "updateMany" && !Aflag) {
                            access.hasAccessToFollowing ? final = {$set: {}} : final = options
                            delete final["$set"]["_id"]
                            delete final["$set"].allowed

                            Aflag = true
                        }

                        if (operation == "find") {
                            access.hasAccessToFollowing ? validObject = {} : validObject = document
                            delete validObject["_id"]
                            delete validObject.allowed
                        }

                        switch (operation) {
                            case "find":
                                for (let field of access.fields)
                                    for (let docField in document)
                                        if (access.hasAccessToFollowing && docField == field)
                                            validObject[field] = document[docField]
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete validObject[field]
                                        }
                                final.push(validObject)
                                break
                            case "updateMany":
                                console.log(access)
                                for (let field of access.fields) {
                                    for (let docField in options["$set"]) {
                                        if (access.hasAccessToFollowing && docField == field) {
                                            if (field != "") final["$set"][field] = options["$set"][field]
                                            console.log(field)
                                        }
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete final["$set"][field]
                                        }
                                    }
                                }
                                break
                            case "deleteMany":
                                console.log(access)
                                if (!access) return (reject({msg: "NOACCESS"}))
                                break
                        }
                    }
                    else return(reject({msg: "NOALLOWEDSECTION"}))
                }

                switch (operation) {
                    case "find":
                        console.log(final)
                        resolve(final)
                        break
                    case "updateMany":
                        connection.db(db).collection(collection).updateMany(query, final).then(
                            () => resolve(),
                            (err) => reject({msg: "NOCONNECTION", err})
                        )
                        break
                    case "deleteMany":
                        connection.db(db).collection(collection).deleteMany(query).then(
                            () => resolve(),
                            (err) => reject({msg: "NOCONNECTION", err})
                        )
                        break
                }
            },
            (err) => reject({msg: "NOCONNECTION", err})
        )
    })
}

function checkConnectionOfOne(userRank, userId, connection, db, collection, query, operation, options) {
    return new Promise((resolve, reject) => {
        connection.db(db).collection(collection).findOne(query).then(
            (document) => {
                if(document) {
                    if (document.allowed) {
                        let access = checkAccess(document, userRank, userId, operation)

                        let validObject = options

                        if (operation == "updateOne") {
                            access.hasAccessToFollowing ? validObject = {$set: {}} : validObject = options
                            delete validObject["$set"]["_id"]
                            delete validObject["$set"].allowed
                        }

                        if (operation == "findOne") {
                            access.hasAccessToFollowing ? validObject = {} : validObject = document
                            delete validObject["_id"]
                            delete validObject.allowed
                        }

                        switch (operation) {
                            case "findOne":
                                for (let field of access.fields)
                                    for (let docField in document)
                                        if (access.hasAccessToFollowing && docField == field)
                                            validObject[field] = document[docField]
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete validObject[field]
                                        }
                                return resolve(validObject)
                                break
                            case "updateOne":
                                for (let field of access.fields) {
                                    for (let docField in options["$set"]) {
                                        if (access.hasAccessToFollowing && docField == field) {
                                            if (field != "") validObject["$set"][field] = options["$set"][field]
                                        }
                                        else if (!access.hasAccessToFollowing && docField == field) {
                                            delete validObject["$set"][field]
                                        }
                                    }
                                }

                                connection.db(db).collection(collection).updateOne(query, validObject).then(
                                    () => resolve(),
                                    (err) => reject({msg: "NOCONNECTION", err})
                                )
                                break
                            case "deleteOne":
                                if (!access) return (reject({msg: "NOACCESS"}))
                                if (query.id == "header") {
                                    delete query.id
                                }
                                else connection.db(db).collection(collection).deleteOne(query).then(
                                    () => resolve(),
                                    (err) => reject({msg: "NOCONNECTION", err})
                                )
                                break
                        }
                    }
                }
                else
                    switch(operation){
                        case "findOne":
                            resolve(null)
                            break
                        default:
                            reject({msg: "NOTHINGFOUND"})
                    }
            },
            (err) => reject({msg: "NOCONNECTION", err})
        )
    })
}

module.exports = (server, app) => {
    server.post("/clientDb", (req, res) => {
        return new Promise(async (resolve, reject) => {
            let rank, userId, op = req.body.operation

            if (req.session.userId) {
                userId = req.session.userId

                await app.dbClient.db("test").collection("userRanks").findOne({"_id": new app.ObjectID(req.session.userId)}).then(
                    (response) => {
                        rank = response.rank
                    },
                    (err) => reject({msg: "NOCONNECTION", err})
                )
            }
            else {
                rank = undefined
                userId = undefined
            }


            if (!req.body.queryData || typeof req.body.queryData != "object") return (reject({msg: "NOQUERY"}))

            if(req.body.queryData.id == "header") {
                return (reject("NOACCESS"))
            }
            if (op == "updateMany" || op == "updateOne") {
                if(!req.body.options || typeof req.body.options != "object") return (reject({msg: "NOOPTIONS"}))
                if(req.body.options["$set"].allowed) return (reject("NOACCESS"))
            }

            checkHeader(rank, app.dbClient, req.body.db, req.body.collection, op).then(
                (resp) => {
                    if (!resp) resp = {read: false, write: false}

                    if (op == "insertMany" || op == "insertOne") {
                        if (resp.write) {
                            return app.dbClient.db(req.body.db).collection(req.body.collection)[op](req.body.queryData).then(
                                () => resolve(true),
                                (err) => reject({msg: "NOCONNECTION", err})
                            )
                        }
                        else reject({msg: "NOACCESS"})
                    }

                    else if (op == "find") {
                        return checkConnectionOfMany(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op, req.query.options).then(
                            (response) => resolve(response),
                            (err) => reject({msg: "NOCONNECTION", err})
                        )
                    }
                    else if (op == "findOne") {
                        return checkConnectionOfOne(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op).then(
                            (response) => resolve(response),
                            (err) => reject({msg: "NOCONNECTION", err}))
                    }
                    else {
                        if (resp.read && resp.write) {
                            return app.dbClient.db(req.body.db).collection(req.body.collection)[op](req.body.queryData, req.body.options).then(
                                () => resolve(true),
                                (err) => reject({msg: "NOCONNECTION", err})
                            )
                        }
                        else {
                            if (op == "deleteMany" || op == "updateMany")
                                return checkConnectionOfMany(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op, req.body.options).then(
                                    () => resolve(),
                                    (err) => reject(err)
                                )
                            if (op == "deleteOne" || op == "updateOne")
                                return checkConnectionOfOne(rank, userId, app.dbClient, req.body.db, req.body.collection, req.body.queryData, op, req.body.options).then(
                                    () => resolve(),
                                    (err) => reject(err)
                                )
                            else reject({msg: "NOCONNECTION", err})
                        }
                    }
                },
                (err) => reject(err)
            )

        }).then(
            (resp2) => {
                return res.send({err: false, response: resp2})
            },
            (err) => {
                let errMsg;
                console.log(err)
                err.msg ? errMsg = err.msg : errMsg = err
                //Need to log err.err
                return res.send({err: true})
            }
        )
    })
}