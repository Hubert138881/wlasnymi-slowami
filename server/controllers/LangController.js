const defaultLanguage = "en", checkBrowserLanguage = true

/**
 * @usage operations on language (routes/language packs)
 *
 * > getPreferredLang
 * @work determines the best language to redirect client to.
 * 1. check if there is a language already chosen by client in cookies
 * 2. set language to the equivalent for used in browser
 * 3. use our main one
 *
 * > "/"
 * @work redirects user to our page with specified language
 *
 * > "/:lang" and "/:lang/*"
 * @work Redirects client to the best language pack (Fragments explained in function body)
 *
 * > "/langPack"
 * @work get the language packs from jsons (default and preferred by user), then fill the gaps in preferred one just by merging with master (default) one
 * @success full language pack
 * @fail error status E1000-1 ({error: "E1000-1"})
 */

function getPreferredLang(req){
    let userLanguage = req.headers["accept-language"].split(";")[0].split(",")[1].split("-")[0].trim()
    let cookieLanguage = req.cookies.lang

    let preferredLang = defaultLanguage

         if (cookieLanguage) preferredLang = cookieLanguage
    else if (userLanguage && userLanguage != "" && checkBrowserLanguage) preferredLang = userLanguage
    return preferredLang
}

module.exports = (server, app) => {
    server.get("/", (req, res) => {
        return res.redirect(`/${getPreferredLang(req)}`)
    })

    server.get("/:lang", (req, res) => {
        let preferredLang = getPreferredLang(req)

        if(!req.cookies.lang) res.cookie('lang', preferredLang, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: false})
        //If the path that user wants differs from the one he provided in the URL we redirect him to the page with a correct language
        if(preferredLang != req.path.split("/")[1]) {
            return res.redirect(`/${preferredLang}`)
        }

        return app.render(req, res, "/")
    })

    server.get("/:lang/*", (req, res) => {
        //There are many objects (redirects) when you use * in the path. Make sure if we operate on the good req with a visible route field (without * like {path: /en/sample})
        if(req.headers["upgrade-insecure-requests"]) {
            let preferredLang = getPreferredLang(req)

            //Split & join old route into the one with specified language (newPath) and the one with language segment thrown away (checkPath)
            if(!req.cookies.lang) res.cookie('lang', preferredLang, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: false})

            let pathSplit = req.path.split("/"), newPath = "/" + preferredLang, checkPath = ""
            for (let i = 2; i < pathSplit.length; i++) {
                checkPath += ("/" + pathSplit[i])
            }
            newPath += checkPath

            //Handle 404 error preventing infinite loop over wrong path just by checking if require route exists on the server
            if (!app.fs.existsSync(`pages/[lang]${checkPath}.js`))  return(app.render(req, res, `/${preferredLang}/errors/E404`))

            //If the language differs from the one we determined for him (f.e user clicked our URL on the chinese page), we simply redirect him to the correct language path and send the language path info to the cookie
            if(preferredLang != pathSplit[1]) {
                res.cookie('lang', preferredLang, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: false})
                return res.redirect(newPath)
            }
        }
        return app.render(req, res, req.path)
    })

    server.post("/langPack", async (req, res) => {
        let full = {langPack: {}, serverData: {}}
        let langPack = {main: {}, default: {}}
        try {
            langPack.main = require(`../langPacks/${req.body.lang}.js`)
            langPack.default = require(`../langPacks/${defaultLanguage}.js`)
            if (defaultLanguage != req.body.lang) Object.assign(langPack.default, langPack.main)
            full.langPack = langPack.default
            full.serverData = app.serverData
        }
        catch (err) {
            app.errorLog.error()
            return (res.send({error: "E1000-1"}))
        }
        return res.send(full)
    })
}