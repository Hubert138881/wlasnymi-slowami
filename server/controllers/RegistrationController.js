const inputValidation = require("../collectors/inputValidation")

module.exports = (server, app) => {
    server.post("/registration", app.upload.fields([]), async (req, res) => {

        return new Promise(async (resolve, reject) => {
            inputValidation(app, req).then(
                () => {
                    app.bcrypt.hash(req.body.password, 10).then(
                        (hash) => {
                            let objectID = new app.ObjectID()

                            app.dbClient.db("test").collection("users").insertOne({
                                _id: objectID,
                                email: req.body.email,
                                username: req.body.username,
                                type: "standard",
                                password: hash,
                                allowed: {
                                    find: [
                                        {"moderator": []},
                                        {[objectID.toHexString()]: []},
                                        ["email", "username", "type"],
                                        ["email", "username", "type"]
                                    ],
                                    update: [
                                        {"moderator": []},
                                        {[objectID.toHexString()]: ["email", "username", "type"]},
                                        [],
                                        []
                                    ],
                                    delete: [
                                        ["moderator"],
                                        [objectID.toHexString()]
                                    ]
                                }
                            }).then(
                                () => {
                                    app.dbClient.db("test").collection("userRanks").insertOne({
                                        _id: objectID,
                                        rank: "standard",
                                        allowed: {
                                            read: [["admin"], [], ["rank"]],
                                            write: [["admin"], [], []]
                                        }
                                    }).then(
                                        () => resolve(),
                                        (err) => reject({msg: "CONNECTIONERROR", err})
                                    )
                                },
                                (err) => reject({msg: "CONNECTIONERROR", err})
                            )
                        },
                        (err) => reject({msg: "HASHERROR", err})
                    )
                },
                (validations) => reject(validations)
            )
        }).then(
            () => res.send({err: false}),
            (err) => {
                //Need to log err.err
                switch(err.msg){
                    case "CONNECTIONERROR":
                        res.send({err: true})
                        break
                    case "INPUTERROR":
                        res.send({err: true, validations: err.err})
                        break
                    case "HASHERROR":
                        res.send({err: true})
                        break
                }
            }
        )
    })
}