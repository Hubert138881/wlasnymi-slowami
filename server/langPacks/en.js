module.exports = {
    "welcome": "Welcome",
    "username": "Username",
    "email": "E-mail",
    "password": "Password",
    "confirmation": "Confirmation",
    "LoginConfirm": "Confirm",

    //error codes
    "E404": "(404) You've got to the road not taken...",
    "E1000": "Language pack Ajax error (Not shown from this file)",
    "E1000-1": "Language pack error connected with node file system (Not shown from this file)",
    "E1001": "Logs error. Something does not work properly",
    "E1002": "Server connection error. Please try once again",
    "E1003": "Ajax error",
    "E1004-1": "Sorry, but we have problems with connection right now!",
    "E1004-2": "Required operation does not exist",
    "E1004-3": "Sorry, but you do not have access to this data",
    "E1004-4": "You have to provide a 'newValues' parameter",
    "E1004-5": "You have to provide a 'query' parameter",
    "E1004-6": "Sorry, but an unexpected error happened to your data",

    //form errors
    "NOTHING": '(input) => `Your ${input} cannot be empty!`',
    "TOOSHORT": '(input, min) => `Your ${input} should contain at least ${min} characters!`',
    "TOOLONG": '(input, min, max) => `Your ${input} should contain no more than ${max} characters!`',
    "ALREADYEXISTS": '(input) => `Sorry, but this ${input} is already occupied!`',
    "WRONGSYNTAX": '(input) => `Your ${input} has a bad structure!`',
    "UNPASSEDCAPTCHA": 'Our detectors received a bot-like behaviour. Access denied. Try to refresh the page',
    "WRONGEMAILORPASSWORD": 'Your e-mail or password is incorrect. Please try again',
    "SAME": '(input) => `Your ${input} is the same as it was!`',
    "NOTSAME": '() => `Sorry but that\'s not your password!`'
}