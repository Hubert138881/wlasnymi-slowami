module.exports = {
    "welcome": "Witamy",
    "username": "Nazwa użytkownika",
    "email": "E-mail",
    "password": "Hasło",
    "LoginConfirm": "Zatwierdź",
    "E404": "(404) Dotarłeś do krańca i początku wieczystej otchłani",

    "NOTHING": '(input) => `Pole ${input} nie może być puste!`',
    "TOOSHORT": '(input, min) => `Pole ${input} powinno mieć przynajmniej ${min} znaków!`',
    "TOOLONG": '(input, min, max) => `Pole ${input} powinno zawierać nie więcej niż ${max} znaków!`',
    "WRONGSYNTAX": '(input) => `Pole ${input} ma złą składnię!`',
    "UNPASSEDCAPTCHA": 'Nasza captcha mówi nam, że jesteś botem. Spróbuj jeszcze raz',
    "WRONGEMAILORPASSWORD": 'Twój e-mail lub hasło są niepoprawne'
}