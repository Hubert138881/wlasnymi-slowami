const express = require("express")
const next = require('next')
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const multer = require('multer')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session);
const MongoClient = require('mongodb').MongoClient

const port = process.env.PORT || 3000,
      dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

//LOG System
const log4js = require('log4js');
log4js.configure({
    appenders: {
        errors: {type: 'file', filename: 'logs/errorLog.log'},
        app: {type: 'file', filename: 'logs/appLog.log'}
    },
    categories: {
        errors: {appenders: ['errors'], level: ['error']},
        default: {appenders: ['app'], level: ['trace']}
    }
})

app.fs = require("fs")
app.request = require("request")
app.errorLog = log4js.getLogger('errors')
app.appLog = log4js.getLogger('app')
app.upload = multer();
app.bcrypt = require("bcrypt")
app.serverData = require("./serverData.js")
app.ObjectID = require('mongodb').ObjectID

const serverUri = `mongodb+srv://${app.serverData.mongoUser}:${app.serverData.mongoPassword}@test-v4mxk.mongodb.net/test?retryWrites=true&w=majority`
const serverClient = new MongoClient(serverUri, { useUnifiedTopology: true, useNewUrlParser: true })
app
    .prepare()
    .then(() => {
        const server = express()

        server.use(bodyParser.json())
        server.use(bodyParser.urlencoded({extended: true}))
        server.use(cookieParser())

        server.use(session({
            secret: 'ASampleSessionSecret',
            store: new MongoStore({url: serverUri}),
            ttl: 40 * 3600,
            saveUninitialized: false,
            resave: false
        }))

        server.use(async (req, res, next) => {
            if(!app.dbClient) app.dbClient = await serverClient.connect()
            next()
        })
        server.use(async (req, res, next) => {
            if(req.session.userId && app.dbClient) {
                if(!req.user)
                    await new Promise((resolve, reject) => {
                        app.dbClient.db("test").collection("users").findOne({"_id": new app.ObjectID(req.session.userId)}).then(
                            (resp) => {
                                if(resp) resolve(resp)
                                else reject({msg: "NOUSERFOUND"})
                            },
                            (err) => reject({msg: "CONNECTIONERROR", err})
                        )
                    }).then(
                        (resp) => {
                            req.user = resp
                        },
                        (err) => {
                            //Errors occured need to log them
                            req.user = {rank: null}
                        }
                    )
            }
            else req.user = {rank: null}
            next()
        })

        var controllerList = ["Log", "Lang", "Database", "Registration", "ChangeUserData", "Login", "Session"]; //Tu nalezy dopisac kolejne kontrolery
        controllerList.map(name => require(`./controllers/${name}Controller`)(server, app))

        server.get("*", (req, res) => {
            return handle(req, res)
        })

        server.listen(port, err => {
            if (err) throw err
            console.log(`Server's ready on ${port}`)
        })
    })
    .catch(ext => {
        console.error(ext.stack)
        process.exit(1)
    })