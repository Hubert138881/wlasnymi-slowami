const Validators = require("./Validators")
module.exports = (app, req) => {
    return new Promise(async (resolveMain, rejectMain) => {
        let validations = {}
        let dbPromises = []
console.log(req.body)
        if(req.body.username) req.body.username = req.body.username.trim().toLowerCase()
        if(req.body.email) req.body.email = req.body.email.trim().toLowerCase()

        for (let field in req.body) {
            switch (field) {
                case "username":
                    validations[field] = Validators[field](req.body[field])
                    dbPromises.push(
                        new Promise((resolve) => {
                            app.dbClient.db("test").collection("users").findOne({username: req.body.username}, (err, resp) => {
                                if (err) rejectMain({msg: "CONNECTIONERROR", err})

                                if (resp) resolve({username: "ALREADYEXISTS"})
                                else resolve({username: false})
                            })
                        })
                    )
                    break
                case "email":
                    validations[field] = Validators[field](req.body[field])
                    dbPromises.push(
                        new Promise((resolve) => {
                            app.dbClient.db("test").collection("users").findOne({email: req.body.email}, (err, resp) => {
                                if (err) rejectMain({msg: "CONNECTIONERROR", err})

                                if (resp) resolve({email: "ALREADYEXISTS"})
                                else resolve({email: false})
                            })
                        })
                    )
                    break
                case "password":
                    validations[field] = Validators[field](req.body[field])
                    break
                case "token":
                    validations[field] = await
                        new Promise((resolve) => {
                            app.request.post({
                                    url: "https://www.google.com/recaptcha/api/siteverify",
                                    form: {
                                        secret: app.serverData.secretKey,
                                        response: req.body.token,
                                        remoteip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                                    }
                                },
                                (err, response, body) => {
                                    if (err) rejectMain({msg: "CONNECTIONERROR", err})

                                    if (!JSON.parse(body).success) resolve("UNPASSEDCAPTCHA")
                                    else resolve(false)
                                })
                        })
                    break
                case "confirmation":
                    dbPromises.push(new Promise((resolve) => {
                        app.bcrypt.compare(req.body[field], req.user.password).then(
                            (res) => {
console.log(res, req.user.password)
                                if(res) resolve({confirmation: false})
                                else resolve({confirmation: "NOTSAME"})
                            },
                            (err) => rejectMain({msg: "HASHERROR", err})
                        )
                    }))
                    break
            }
        }

        await Promise.all(dbPromises).then((data) => {
            for (let dbInfo of data)
                for (let field in dbInfo)
                    if (dbInfo[field])
                        validations[field] = dbInfo[field]
        })

        for(let field in validations)
            if(validations[field])
                rejectMain({msg: "INPUTERROR", err: validations})

        resolveMain()
    })
}