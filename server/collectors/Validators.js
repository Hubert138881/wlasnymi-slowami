const inputLengthRange = {
    username: {
        min: 4,
        max: 15
    },
    email: {
        min: 5
    },
    password: {
        min: 8,
        max: 64
    }
}

module.exports = class Validators {
    static username(content) {
        content = content.trim()

        let regex = /^([a-zA-Z0-9])+$/
        if (content == "") return "NOTHING"
        if (content.length < inputLengthRange.username.min) return "TOOSHORT"
        if (content.length > inputLengthRange.username.max) return "TOOLONG"
        if (!content.match(regex)) return "WRONGSYNTAX"
        return false
    }

    static email(content) {
        content = content.trim()

        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (content == "") return "NOTHING"
        if (content.length < inputLengthRange.email.min) return "TOOSHORT"
        if (!content.match(regex)) return "WRONGSYNTAX"
        return false
    }

    static password(content) {
        content = content.trim()

        if (content == "") return "NOTHING"
        if (content.length < inputLengthRange.password.min) return "TOOSHORT"
        if (content.length > inputLengthRange.password.max) return "TOOLONG"
        return false
    }
}