/*
How to make layouts:

export default (content) => {
    return <p>{content}</p>
}

USAGE in module:

import layout from "sampleLayout"

class Sample extends Component {
    render() {
        return <>
            {layout("Sample text")}
               </>
    }
}

*/