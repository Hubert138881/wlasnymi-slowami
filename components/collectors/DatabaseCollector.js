import axios from "axios"

export default class Database {
    lang = {}

    constructor(lang){
        this.lang = lang
    }

    async connect(db, collection, operation, queryData, options){
        let serverPath = ""
        if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

        return await new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: `${serverPath}/clientDb`,
                data: {
                    db, collection, operation, queryData, options
                }
            }).then(res => res.data).then(
                res => {
                    if(res.err) reject()
                    else resolve(res.response)
                }
            ).catch((err) => {
                reject()
                //save err 1003 to logs
            })
        })
    }

    findOne(db, collection, queryData){
        return this.connect(db, collection, "findOne", queryData)
    }

    insertOne(db, collection, queryData){
        return this.connect(db, collection, "insertMany", queryData)
    }

    updateOne(db, collection, queryData, toUpdate){
        return this.connect(db, collection, "updateOne", queryData, toUpdate)
    }

    deleteOne(db, collection, queryData){
        return this.connect(db, collection, "deleteOne", queryData)
    }

    find(db, collection, queryData, options){
        return this.connect(db, collection, "find", queryData, options)
    }

    insert(db, collection, queryData){
        return this.connect(db, collection, "insertMany", queryData)
    }

    update(db, collection, queryData, toUpdate){
        return this.connect(db, collection, "updateMany", queryData, toUpdate)
    }

    delete(db, collection, queryData){
        return this.connect(db, collection, "deleteMany", queryData)
    }
}