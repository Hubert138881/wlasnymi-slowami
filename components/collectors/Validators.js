export const inputLengthRange = {
    username: {
        min: 4,
        max: 15
    },
    email: {
        min: 5,
        max: 0
    },
    password: {
        min: 8,
        max: 64
    },
    confirmation: {
        min: 0,
        max: 0
    }
}

export default class Validators {
    static email(lang, name, content) {
        content = content.trim()
        name = name.toLowerCase()

        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (content == "") return eval(lang.NOTHING)(name)
        if (content.length < inputLengthRange.email.min) return eval(lang.TOOSHORT)(name, inputLengthRange.email.min)
        if (!content.match(regex)) return eval(lang.WRONGSYNTAX)(name)
        return false
    }

    static username(lang, name, content) {
        content = content.trim()
        name = name.toLowerCase()

        let regex = /^([a-zA-Z0-9])+$/
        if (content == "") return eval(lang.NOTHING)(name)
        if (content.length < inputLengthRange.username.min) return eval(lang.TOOSHORT)(name, inputLengthRange.username.min)
        if (content.length > inputLengthRange.username.max) return eval(lang.TOOLONG)(name, inputLengthRange.username.min, inputLengthRange.username.max)
        if (!content.match(regex)) return eval(lang.WRONGSYNTAX)(name)
        return false
    }

    static password(lang, name, content) {
        content = content.trim()
        name = name.toLowerCase()

        if (content == "") return eval(lang.NOTHING)(name)
        if (content.length < inputLengthRange.password.min) return eval(lang.TOOSHORT)(name, inputLengthRange.password.min)
        if (content.length > inputLengthRange.password.max) return eval(lang.TOOLONG)(name, inputLengthRange.password.min, inputLengthRange.password.max)
        return false
    }
    static confirmation(lang, name, content) {
        content = content.trim()
        if (content == "") return eval(lang.NOTHING)(name)
        return false
    }
}