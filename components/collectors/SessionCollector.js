import axios from "axios"

export default class Database {
    static connect(operation){
        let serverPath = ""
        if(process.env.npm_package_proxy) serverPath = process.env.npm_package_proxy

        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: `${serverPath}/sessionControl`,
                data: {
                    operation
                }
            }).then(res => res.data).then(
                res => {
                    if(res.err) reject()
                    else resolve()
                }
            ).catch((err) => {
                reject()
                //save err 1003 to logs
            })
        })
    }

    static delete(){
        return this.connect("delete")
    }
    static regenerate() {
        return this.connect("regenerate")
    }
}