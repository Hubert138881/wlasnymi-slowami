"use strict"

import React, {Component} from 'react'
import axios from 'axios'

import Validators, {inputLengthRange} from "./collectors/Validators"
import Database from "./collectors/DatabaseCollector"

export default class RegistrationForm extends Component {
    connection = null

    state = {
        disableSubmit: true,
        disableInputs: false,

        exceptions: {
            username: eval(this.props.langPack.NOTHING)(this.props.langPack.username.toLowerCase()),
            email: eval(this.props.langPack.NOTHING)(this.props.langPack.email.toLowerCase()),
            password: eval(this.props.langPack.NOTHING)(this.props.langPack.password.toLowerCase()),
            checkbox1: true
        },
        exceptionsToShow: {}
    }

    constructor(props){
        super(props)
        this.connection = new Database(props)
    }

    async submitForm(e){
        e.preventDefault()
        var target = e.target
        if(!this.state.disableSubmit){
            let formData = new FormData(target)

            this.setState({disableSubmit: true, disableInputs: true})
            return new Promise((resolve, reject) => {
                grecaptcha.ready(() => {
                    grecaptcha.execute(this.props.serverData.siteKey).then(async (token) => {
                        formData.append("token", token)

                        await axios({
                            method: "POST",
                            url: "/registration",
                            data: formData
                        }).then(res => res.data)
                            .then((response) => {
                                if(response.err) {
                                    if(response.validations) {
                                        for(let field in response.validations){
                                            if(response.validations[field]) {
                                                if(field != "token") {
                                                    let errorContent = eval(this.props.langPack[response.validations[field]])(this.props.langPack[field].toLowerCase(), inputLengthRange[field].min, inputLengthRange[field].max)

                                                    this.setObjectInState("exceptions", {[field]: errorContent})
                                                    this.setObjectInState("exceptionsToShow", {[field]: errorContent})
                                                }
                                                else this.setObjectInState("exceptionsToShow", {fatal: this.props.langPack[response.validations[field]]})
                                            }
                                        }
                                    }
                                    else this.setObjectInState("exceptionsToShow", {fatal: "Sample message"})

                                    reject()
                                }
                                else resolve()
                            })
                            .catch((err) => {
                                this.setObjectInState("exceptionsToShow", {fatal: this.props.langPack.E1002})
                                reject(err)
                            })
                    })
                })
            }).then(
                () => {
                    return console.log("noError")
                },
                (err) => {
                    //Need to log err
                    for(let input of target) {
                        if(!input.type) return(false)
                        if(input.type == "password") input.value = ""
                        if(input.type == "checkbox") input.checked = false

                        this.setState({disableInputs: false})
                    }
                }
            )
        }
    }

    setObjectInState(inStateObjectName, toSave){
        let inStateObject = this.state[inStateObjectName]
        Object.assign(inStateObject, toSave)
        this.setState({[inStateObjectName]: inStateObject})
    }

    checkIfExceptions() {
        for(let exc in this.state.exceptions) if(this.state.exceptions[exc]) return(true)
        return false
    }

    render(){
        return(
            <>
                <script src={`https://www.google.com/recaptcha/api.js?render=${this.props.serverData.siteKey}`}></script>
                <form onSubmit={(e) => this.submitForm(e)}>
                    <label>
                        <input name={"username"} disabled={this.state.disableInputs}
                               onChange={async (e) => {
                                   await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.langPack, this.props.langPack[e.target.name], e.target.value)})
                                   this.checkIfExceptions() ? this.setState({disableSubmit: true}) : this.setState({disableSubmit: false})
                               }}
                               onBlur={async (e) => {
                                   e.persist()
                                   if (!this.state.exceptions[e.target.name])
                                       await new Promise((resolve) => {
                                           this.connection.findOne("test", "users", {["username"]: e.target.value.trim().toLowerCase()}).then(
                                               (resp) => {
                                                   if (resp) this.setObjectInState("exceptions", {username: eval(this.props.langPack["ALREADYEXISTS"])("username")})
                                                   resolve()
                                               },
                                               () => {
                                                   this.setObjectInState("exceptionsToShow", {fatal: "Unexpected error!"})
                                                   resolve()
                                               }
                                           )
                                       })

                                   if (this.state.exceptions[e.target.name]) this.setObjectInState("exceptionsToShow", {[e.target.name]: this.state.exceptions[e.target.name]})
                               }}
                               onFocus={(e) => {
                                   if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                               }}
                               placeholder={this.props.langPack.username}/>
                        <div id={"emailError"}>{this.state.exceptionsToShow.username}</div>
                    </label>
                    <label>
                        <input name={"email"} disabled={this.state.disableInputs}
                               onChange={async (e) => {
                                   await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.langPack, this.props.langPack[e.target.name], e.target.value)})
                                   this.checkIfExceptions() ? this.setState({disableSubmit: true}) : this.setState({disableSubmit: false})
                               }}
                               onBlur={async (e) => {
                                   e.persist()
                                   if (!this.state.exceptions[e.target.name])
                                       await new Promise((resolve) => {
                                           this.connection.findOne("test", "users", {["email"]: e.target.value.trim().toLowerCase()}).then(
                                               (resp) => {
                                                   if (resp) this.setObjectInState("exceptions", {email: eval(this.props.langPack["ALREADYEXISTS"])("email")})
                                                   resolve()
                                               },
                                               () => {
                                                   this.setObjectInState("exceptionsToShow", {fatal: "Unexpected error!"})
                                                   resolve()
                                               }
                                           )
                                       })

                                   if (this.state.exceptions[e.target.name]) this.setObjectInState("exceptionsToShow", {[e.target.name]: this.state.exceptions[e.target.name]})

                               }}
                               onFocus={(e) => {
                                   if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                               }}
                               placeholder={this.props.langPack.email}/>
                        <div id={"emailError"}>{this.state.exceptionsToShow.email}</div>
                    </label>
                    <label>
                        <input name={"password"} type={"password"} disabled={this.state.disableInputs}
                               onChange={async (e) => {
                                   await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.langPack, this.props.langPack[e.target.name], e.target.value)})
                                   this.checkIfExceptions() ? this.setState({disableSubmit: true}) : this.setState({disableSubmit: false})
                               }}
                               onBlur={(e) => {
                                   if (this.state.exceptions[e.target.name]) this.setObjectInState("exceptionsToShow", {[e.target.name]: this.state.exceptions[e.target.name]})
                               }}
                               onFocus={(e) => {
                                   if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                               }}
                               placeholder={this.props.langPack.password}/>
                        <div id={"passwordError"}>{this.state.exceptionsToShow.password}</div>
                    </label>

                    <label style={{display: "flex"}}><input type={"checkbox"} name={"checkbox1"} disabled={this.state.disableInputs}
                    onChange={async (e) => {
                        await this.setObjectInState("exceptions", {[e.target.name]: !e.target.checked})
                        this.checkIfExceptions() ? this.setState({disableSubmit: true}) : this.setState({disableSubmit: false})
                    }}
                    ></input><p>Agreement number 1</p></label>

                    <label>
                        <div id={"fatalError"}>{this.state.exceptionsToShow.fatal}</div>
                        <button disabled={this.state.disableSubmit}> {this.props.langPack.LoginConfirm}</button>
                    </label>
                </form>
            </>
        )
    }
}