import React, {Component} from 'react'
import Validators, {inputLengthRange} from "./collectors/Validators"
import Database from "./collectors/DatabaseCollector"
import axios from "axios/index"

export default class PanelToChangeUserData extends Component {
    connection = new Database(this.props.langPack)

    state = {
        confirmationPanel: {
            show: false,
            inputVal: undefined,
            inputType: undefined
        },

        inputValues: {
            username: this.props.userData.username,
            email: this.props.userData.email
        },

        disableInputs: {
            username: false,
            email: false,
            password: false,
            confirmation: false
        },

        disableSubmit: {
            username: true,
            email: true,
            password: true,
            confirmation: true
        },

        exceptions: {
            username: eval(this.props.langPack.SAME)(this.props.langPack.username.toLowerCase()),
            email: eval(this.props.langPack.SAME)(this.props.langPack.email.toLowerCase()),
            password: eval(this.props.langPack.NOTHING)(this.props.langPack.password.toLowerCase()),
            confirmation: eval(this.props.langPack.NOTHING)(this.props.langPack.confirmation.toLowerCase()),
            fatal: false
        },
        exceptionsToShow: {}
    }

    setObjectInState(inStateObjectName, toSave) {
        let inStateObject = this.state[inStateObjectName]
        Object.assign(inStateObject, toSave)
        this.setState({[inStateObjectName]: inStateObject})
    }

    render() {
        return(
            <div>
                <b>Change your password</b>
                <form onSubmit={(e) => {
                    e.preventDefault()
                    this.setObjectInState("confirmationPanel", {show: true, inputType: e.target[0].name, inputVal: new FormData(e.target)})
                }}>
                    <input name={"password"} type={"password"} disabled={this.state.disableInputs.password}
                           onChange={async (e) => {
                               e.persist()
                               await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.langPack, this.props.langPack[e.target.name], e.target.value)})
                               if(this.state.exceptions[e.target.name]) this.setObjectInState("disableSubmit", {[e.target.value]: true})
                               else this.setObjectInState("disableSubmit", {[e.target.name]: false})
                           }}
                           onBlur={(e) => {
                               if (this.state.exceptions[e.target.name]) this.setObjectInState("exceptionsToShow", {[e.target.name]: this.state.exceptions[e.target.name]})
                           }}
                           onFocus={(e) => {
                               if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                           }}
                           placeholder={this.props.langPack.password}/>
                    <div id={"passwordError"}>{this.state.exceptionsToShow.password}</div>
                    <button disabled={this.state.disableSubmit.password}>Confirm</button>
                </form>
            <br/>
                <b>Change your e-mail</b>
                <form onSubmit={(e) => {
                    e.preventDefault()
                    this.setObjectInState("confirmationPanel", {show: true, inputType: e.target[0].name, inputVal: new FormData(e.target)})
                }}>
                    <input name={"email"} disabled={this.state.disableInputs.email}
                           onChange={async (e) => {
                               e.persist()
                               this.setObjectInState("disableSubmit", {[e.target.name]: true})
                               this.setObjectInState("inputValues", {[e.target.name]: e.target.value})
                               if(e.target.value.toLowerCase().trim() == this.props.userData[e.target.name])
                                   this.setObjectInState("exceptions", {[e.target.name]: eval(this.props.langPack.SAME)(this.props.langPack[e.target.name].toLowerCase())})
                               else await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.langPack, this.props.langPack[e.target.name], e.target.value)})
                           }}
                           value={this.state.inputValues.email}
                           onBlur={async (e) => {
                               e.persist()
                               if (!this.state.exceptions[e.target.name])
                                   await new Promise((resolve) => {
                                   this.connection.findOne("test", "users", {["email"]: e.target.value.trim().toLowerCase()}).then(
                                       (resp) => {
                                           if (resp) this.setObjectInState("exceptions", {email: eval(this.props.langPack["ALREADYEXISTS"])("email")})
                                           else this.setObjectInState("disableSubmit", {[e.target.name]: false})
                                           resolve()
                                       },
                                       () => {
                                           this.setObjectInState("exceptionsToShow", {fatal: "Unexpected error!"})
                                           resolve()
                                       }
                                   )
                               })
                               if (this.state.exceptions[e.target.name]) this.setObjectInState("exceptionsToShow", {[e.target.name]: this.state.exceptions[e.target.name]})
                           }}
                           onFocus={(e) => {
                               if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                           }}/>

                    <div id={"emailError"}>{this.state.exceptionsToShow.email}</div>
                    <button disabled={this.state.disableSubmit.email}>Confirm</button>
                </form>
            <br/>
                <b>Change your username</b>
                <form onSubmit={(e) => {
                    e.preventDefault()
                    this.setObjectInState("confirmationPanel", {show: true, inputType: e.target[0].name, inputVal: new FormData(e.target)})
                }}>
                    <input name={"username"} disabled={this.state.disableInputs.username}
                           onChange={async (e) => {
                               e.persist()
                               this.setObjectInState("disableSubmit", {[e.target.name]: true})
                               this.setObjectInState("inputValues", {[e.target.name]: e.target.value})
                               if(e.target.value.toLowerCase().trim() == this.props.userData[e.target.name])
                                   this.setObjectInState("exceptions", {[e.target.name]: eval(this.props.langPack.SAME)(this.props.langPack[e.target.name].toLowerCase())})
                               else await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.langPack, this.props.langPack[e.target.name], e.target.value)})
                           }}
                           value={this.state.inputValues.username}
                           onBlur={async (e) => {
                               e.persist()
                               if (!this.state.exceptions[e.target.name])
                                   await new Promise((resolve) => {
                                       this.connection.findOne("test", "users", {["username"]: e.target.value.trim().toLowerCase()}).then(
                                           (resp) => {
                                               if (resp) this.setObjectInState("exceptions", {username: eval(this.props.langPack["ALREADYEXISTS"])("username")})
                                               else this.setObjectInState("disableSubmit", {[e.target.name]: false})
                                               resolve()
                                           },
                                           () => {
                                               this.setObjectInState("exceptionsToShow", {fatal: "Unexpected error!"})
                                               resolve()
                                           }
                                       )
                                   })
                               if (this.state.exceptions[e.target.name]) this.setObjectInState("exceptionsToShow", {[e.target.name]: this.state.exceptions[e.target.name]})
                           }}
                           onFocus={(e) => {
                               if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                           }}/>

                    <div id={"usernameError"}>{this.state.exceptionsToShow.username}</div>
                    <button disabled={this.state.disableSubmit.username}>Confirm</button>
                </form>
                <div id={"fatalError"}>{this.state.exceptionsToShow.fatal}</div>

                {this.state.confirmationPanel.show ?
                    <form onSubmit={(e) => {
                        e.preventDefault()
                        this.state.confirmationPanel.inputVal.delete(e.target[0].name)
                        this.state.confirmationPanel.inputVal.append(e.target[0].name, e.target[0].value)
                        this.submitForm()
                    }}>
                        <div onClick={() => this.setObjectInState("confirmationPanel", {show: false, inputType: undefined, inputVal: undefined})}>Hide</div>
                        <h1>Please, confirm changes by entering your recent password</h1>
                        <input name={"confirmation"} type={"password"} placeholder={"Your recent password"}
                               disabled={this.state.disableInputs.confirmation}
                               onChange={async (e) => {
                                   e.persist()
                                   await this.setObjectInState("exceptions", {[e.target.name]: Validators[e.target.name](this.props.langPack, this.props.langPack[e.target.name], e.target.value)})

                                   if(this.state.exceptions[e.target.name]) await this.setObjectInState("disableSubmit", {[e.target.name]: true})
                                   else await this.setObjectInState("disableSubmit", {[e.target.name]: false})
                               }}
                               onBlur={(e) => {
                                   if (this.state.exceptions[e.target.name]) this.setObjectInState("exceptionsToShow", {[e.target.name]: this.state.exceptions[e.target.name]})
                               }}
                               onFocus={(e) => {
                                   if (this.state.exceptionsToShow[e.target.name] != "") this.setObjectInState("exceptionsToShow", {[e.target.name]: ""})
                               }}
                               placeholder={"Your recent password"} />
                        <div id={"confirmationError"}>{this.state.exceptionsToShow.confirmation}</div>
                        <button disabled={this.state.disableSubmit.confirmation}>Confirm</button>
                    </form>
                : false}
            </div>
        )
    }

    submitForm() {
        let inputType = this.state.confirmationPanel.inputType,
            inputVal = this.state.confirmationPanel.inputVal

        this.setObjectInState("disableInputs", {[inputType]: true})
        this.setObjectInState("disableSubmit", {[inputType]: true})

        this.setObjectInState("disableInputs", {confirmation: true})
        this.setObjectInState("disableSubmit", {confirmation: true})

        return new Promise(async (resolve, reject) => {
            await axios({
                method: "POST",
                url: "/changeUserData",
                data: inputVal
            }).then(res => res.data)
                .then((response) => {
                    if(response.err) {
                        if(response.validations) {
                            console.log(response.validations)
                            for(let field in response.validations){
                                if(response.validations[field]) {
                                    if(field != "confirmation")
                                        this.setObjectInState("confirmationPanel", {show: false, inputVal: undefined})
                                    let errorContent = eval(this.props.langPack[response.validations[field]])(this.props.langPack[field].toLowerCase(), inputLengthRange[field].min, inputLengthRange[field].max)
                                    this.setObjectInState("exceptions", {[field]: errorContent})
                                    this.setObjectInState("exceptionsToShow", {[field]: errorContent})
                                }
                            }
                        }
                        else this.setObjectInState("exceptionsToShow", {fatal: "Sample message"})

                        reject()
                    }
                    else resolve()
                }).catch((err) => {
                    //Need to log err
                    this.setObjectInState("exceptionsToShow", {"fatal": this.props.langPack.E1002})
                    return reject()
                })
        }).then(
            () => {
                this.setObjectInState("confirmationPanel", {show: false, inputType: undefined, inputVal: undefined})
                alert("New data have been set successfully!")
            },
            () => {
                this.setObjectInState("disableInputs", {confirmation: false})
                this.setObjectInState("disableSubmit", {confirmation: false})

                /*for (let input of target) {
                    if (!input.type) return (false)
                    if (input.type == "password") input.value = ""
                    if (input.type == "checkbox") input.checked = false
                }*/
            }
        ).finally(() => {
            this.setObjectInState("disableInputs", {[inputType]: false})
            this.setObjectInState("disableSubmit", {[inputType]: false})
        })
    }
}