import React from "react"
import RegistrationForm from "../../components/RegistrationForm"

const Registration = (props) => {
    return <RegistrationForm {...props}/>
}
Registration.getInitialProps = async (props) => {
    return {lang: props.req.params.lang}
}

export default Registration