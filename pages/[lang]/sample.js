import React from 'react'
import Lang from "../../components/collectors/LangCollector"

const Sample = (props) => {
    return <>
        <label id={"pl"} onClick = {() => {Lang.setLang("pl", 30)}}>pl</label>
        <br/><br/>
        <label id={"en"} onClick = {() => Lang.setLang("en", 30)}>en</label>
    </>
}

Sample.getInitialProps = async (props) => {
    return {lang: props.req.params.lang}
}

 export default Sample