import React from "react"
import LoginForm from "../../components/LoginForm"
import Session from "../../components/collectors/SessionCollector"
import A from "../../components/collectors/DatabaseCollector"

const Login = (props) => {

    let a = new A(props)
    /*a.updateOne("test", "users", {id: "hubert13888"}, {$set: {"email": "hubert13888"}}).then(
        (res) => {console.log("Response: ", res)},
        (err) => {console.log("Error: ", err)}
    )*/

    if(props.userData["_id"]) return(<i>Witaj: {props.userData.email}<button onClick={() => Session.delete().then(
        () => {
            alert("Wylogowano")
            location.reload()
        },
        () => {
            alert("Blad przy wylogowywaniu")
        }
    )}>Logout</button></i>)
    else return (<LoginForm {...props}/>)
}

Login.getInitialProps = async (props) => {
    return {lang: props.req.params.lang}
}

export default Login