import React, {useEffect} from "react"
import PanelsToChangeUserData from "../../components/PanelsToChangeUserData"
import A from "../../components/collectors/DatabaseCollector"

const ChangeUserData = (props) => {
    let a = new A(props)

    useEffect(() => {
        if(!props.userData["_id"]) location.href = "login"
    })

    if(props.userData["_id"]) return (<PanelsToChangeUserData {...props}/>)
    else return (<></>)
}

ChangeUserData.getInitialProps = async (props) => {
    return {lang: props.req.params.lang}
}

export default ChangeUserData